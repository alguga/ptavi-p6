"""
Programa cliente que abre un socket a un servidor
"""

import socket
import sys

try:
    metoth = sys.argv[1]
    if metoth != "INVITE" \
            and metoth != "BYE" or len(sys.argv) != 3:
        raise ValueError
    direction = sys.argv[2]
    log_in = direction.split("@")[0]
    IP = direction.split("@")[1].split(":")[0]
    PORT = int(direction.split("@")[1].split(":")[1])
    Cabecera = metoth + " sip:" + (
        direction.split(":")[0]) + " SIP/2.0" + "\r\n"
    LINE = Cabecera
    if metoth == "INVITE":
        """
        Arma el paquete INVITE para iniciar la conversacion correctamente
        """

        Cuerpo = ""
        Cuerpo = Cuerpo + "\r\n"
        Cuerpo = Cuerpo + "v=0" + "\r\n"
        Cuerpo = Cuerpo + "o=robin@gotham.com 127.0.0.1" + "\r\n"
        Cuerpo = Cuerpo + "s=misesion" + "\r\n"
        Cuerpo = Cuerpo + "t=0" + "\r\n"
        Cuerpo = Cuerpo + "m=audio " + str(PORT * 2) + " RTP"
        Cabecera = Cabecera + "Content-Length: " + str(
            len(Cuerpo) + 2) + "\r\n"
        LINE = Cabecera + Cuerpo
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            my_socket.connect((IP, PORT))

            my_socket.send(bytes(LINE, 'utf-8') + b'\r\n')
            data = my_socket.recv(1024)
            print('Recibido -- ', data.decode('utf-8'))

            confirmation = (data.decode('utf-8')).split("   ")
            if confirmation[2] == "SIP/2.0 200 OK":
                LINE = "ACK" + " sip:" + \
                       (direction.split(":")[0]) + " SIP/2.0"
                my_socket.send(bytes(LINE, 'utf-8') + b'\r\n')
                data = my_socket.recv(1024)

    else:
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            my_socket.connect((IP, PORT))
            my_socket.send(bytes(LINE, 'utf-8') + b'\r\n')
            data = my_socket.recv(1024)
            print('Recibido -- ', data.decode('utf-8'))

except (ValueError, IndexError):
    print("Usage: python3 client.py method receiver@IP:SIPport")
