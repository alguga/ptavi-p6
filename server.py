#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

import secrets
import simplertp
import socketserver
import sys

con_port = 0
con_IP = ""


class EchoHandler(socketserver.DatagramRequestHandler):
    """ Gestiona la llegada de paquetes del destinatario"""

    def handle(self):

        linea = ""
        while 1:
            """ Bucle que lee los mensajes que recibe del cliente"""
            line = self.rfile.read()
            try:
                linea = linea + line.decode('utf-8')
            except ValueError:
                linea = linea + ""

            # Si no hay más líneas salimos del bucle infinito
            if not line:
                break
        print("Se ha recibido: " + linea)
        message = []
        global con_port
        global con_IP
        message.append(linea[0: linea.index(" ")])
        message.append(linea[linea.index(":") + 1: linea.index("@")])
        message.append(linea[linea.index("@") + 1: linea.index("\n") - 9])
        if message[0] == "INVITE":

            """Como debe actuar el programa si recibe un invite"""
            self.wfile.write(b"SIP/2.0 100 Trying   "
                             b"SIP/2.0 180 Ringing   SIP/2.0 200 OK")
            con_port = int(linea[linea.index("audio") + 6:
                                 linea.index("RTP") - 1])
            con_IP = (linea[linea.index("o="): linea.index("s=")])
            con_IP = con_IP[con_IP.index(" ") + 1: -2]

        elif message[0] == "ACK":

            """Como debe actuar el programa si recibe un ACK"""
            BIT = secrets.randbits(1)
            RTP_header = simplertp.RtpHeader()
            RTP_header.set_header(version=2, marker=BIT,
                                  payload_type=14, ssrc=200002)
            audio = simplertp.RtpPayloadMp3(FILE)
            simplertp.send_rtp_packet(RTP_header, audio, con_IP, con_port)

        elif message[0] == "BYE":
            """Como debe actuar el programa si recibe un BYE"""
            self.wfile.write(b"SIP/2.0 200 OK")

        elif message[0] != "INVITE" or "ACK" or "BYE" \
                and len(message) == 2:
            self.wfile.write(b"SIP/2.0 405 Method Not Allowed")

        elif len(message) != 2:
            self.wfile.write(b"SIP/2.0 400 Bad Request")

        else:
            self.wfile.write(b"SIP/2.0 400 Bad Request")


if __name__ == "__main__":

    try:
        """Recoge la informacion con la que se conectara al cliente"""
        IP = sys.argv[1]
        PORT = int(sys.argv[2])
        FILE = sys.argv[3]
        file_object = open(FILE, "r")
        file_object.close()
        serv = socketserver.UDPServer((IP, PORT), EchoHandler)
        print("Listening...")
        serv.serve_forever()

    except (ValueError, IndexError, FileNotFoundError):
        print("Usage: python3 server.py IP port audio_file")
